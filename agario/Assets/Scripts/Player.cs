﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	void OnCollisionEnter2D(Collider2D other) {
		if (other.gameObject.CompareTag("Player"))
			Destroy(other.gameObject);
		
	}
}