﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
public class Movement : NetworkBehaviour {
	[SyncVar]
	public float moveSpeed;
	Vector3 Clamp;

	void Update()
	{
		if (isLocalPlayer) 
		{
			Vector3 pointer = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			pointer.z = transform.position.z;
			Clamp = new Vector3 (Mathf.Clamp (transform.position.x, -28, 33.5f), Mathf.Clamp (transform.position.y, -28.5f, 29), 0f);
			transform.position = Vector3.MoveTowards (Clamp, pointer, moveSpeed * Time.deltaTime / transform.localScale.x);	

			Camera.main.transform.position = new Vector3 (transform.position.x, transform.position.y, Camera.main.transform.position.z);

		}
	}
}