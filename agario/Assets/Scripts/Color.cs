﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
public class Color : NetworkBehaviour {
	//[SyncVar]
	public List<Material> Textures = new List<Material>();

	void Awake()
	{
		GetComponent<Renderer> ().material = Textures [Random.Range (0, Textures.Count)];
	}
}