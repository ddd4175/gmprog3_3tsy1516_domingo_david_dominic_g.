﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;


public class NetworkManagerCustom : NetworkManager {

	public InputField txtIPAddress;
	public InputField txtName;

	public void StartUpHost()
	{
		NetworkManager.singleton.StartHost ();
	}

	public void ConnectClient()
	{
		SetIPAddress ();
		NetworkManager.singleton.StartClient ();
	}


	void SetIPAddress()
	{
		NetworkManager.singleton.networkAddress = txtIPAddress.text;
	}
}
