﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;
public class Food : NetworkBehaviour {
	[SyncVar]
	public float mass;
	public GameObject Player;
	public GameObject Split;
	public GameObject Shoot;
	public float size;
	public int score;
	private Vector3 mousePosition;
	public float moveSpeed = 0.05f;
	public float Camerasize = 11f;
	// Use this for initialization
	void Start () {
	
	}
	void OnTriggerEnter(Collider hit) {

		if (hit.gameObject.tag == "Food") {

			GameObject.Destroy (hit.gameObject);
			transform.localScale = new Vector3 (transform.localScale.x + mass, transform.localScale.y + mass, 0);
			score += 10;
			size += 10;
			Camera.main.orthographicSize = Camerasize;
			Camerasize += 0.3f;
		}

	}

	
	// Update is called once per frame
	void Update () {
			
		 if (Input.GetKeyDown("space"))
		{
			if (size > 10) {
				size /= 2;
				//transform.position = Vector2.Lerp (transform.position, mousePosition, moveSpeed);
				//Vector3 target = Camera.main.ScreenToWorldPoint (Input.mousePosition);
				transform.localScale = new Vector3 (transform.localScale.x / 2.0f + 1f, transform.localScale.y / 2.0f + 1f, 0f);
				Split = Instantiate (Split, transform.position, transform.rotation) as GameObject;
				Split.transform.parent = Player.transform;
				//Split.transform.position = Vector3.Lerp(Split.transform.position, target, Time.deltaTime * 10);
			}

		}
			if (Input.GetKeyDown("w"))
		{
			if (size > 10) {
				size -= 10;

				Vector3 target = Camera.main.ScreenToWorldPoint (Input.mousePosition);
				transform.localScale = new Vector3 (transform.localScale.x / 1.5f, transform.localScale.y / 1.5f, 0f);
				Shoot = Instantiate (Shoot, target, Quaternion.identity) as GameObject;
				//Split.transform.position = Vector3.Lerp(Split.transform.position, target, Time.deltaTime * 10);
			}

		}


	}
		
//	void OnGUI()
//	{
//		GUI.TextField(new Rect(10,200,100,50),"Score:" + score);
//	}
}